# -*- coding:UTF-8 -*-
import json
import sys, getopt
import time

cur_week_id = 41
cur_week_type = 0 # 0: even number week 1: odd number week
week_type_name = ['双周' ,'单周']
day_charactor = ['日', '一', '二', '三', '四', '五', '六']

class Date_Info:
    day_id_in_week = 0
    week_type = 0

    def __init__(self, date, week_type):
        self.day_id_in_week = date
        self.week_type = week_type


def _do_parse_(json_file):
    with open(json_file) as jsf:
        jsf_data = json.load(jsf)

    return jsf_data

def _parse_cmd(argv):
    ret = ""

    try:
        opts, args = getopt.getopt(argv, "hi:")
    except getopt.GetoptError:
        print("-i <input_file>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-i':
            ret = arg
        else:
            print("-i <input_file>")
    
    return ret

def __parse_week_type__():
    # global cur_week_id
    # global cur_week_type
    # global week_type_name

    date_info = Date_Info(0, 0)

    date_info.day_id_in_week = int( time.strftime("%w") )

    week_id = int( time.strftime("%W") )

    week_difference_value = week_id - cur_week_id

    date_info.week_type = (cur_week_type + week_difference_value) % 2

    return date_info

def __get_today_course__():
    input_file = _parse_cmd(sys.argv[1:])

    if(input_file == ""):
        print("no parameter detected, exit")
        sys.exit(2)

    print("read:\"" + input_file + "\"...")

    jsf_data = _do_parse_(input_file)

    # print(jsf_data['course'])
    
    date_info = __parse_week_type__()

    cur_day_course_info_header = "星期" + day_charactor[date_info.day_id_in_week] + "," + week_type_name[date_info.week_type] + ":"

    cur_course_info = [cur_day_course_info_header]

    for item in jsf_data['course']:
        if(item['day'] == date_info.day_id_in_week):
            if(item['week_type'] == 2 or item['week_type'] == date_info.week_type):
               cur_course_info.append(item['name'] + ", " + item['time'] + ", " + item['class room'])
    
    if len(cur_course_info) == 1:
        cur_course_info.append("No course tody")

#		#debug print
#    for info in cur_course_info:
#        print(info)

    return cur_course_info

# __get_today_course__()
