# -*- coding: UTF-8 -*-
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import tools
import sys

#reset encoding to utf-8, otherwise omitte ascii encode will lead to several error
reload(sys)
sys.setdefaultencoding("utf-8")

sender = 'server@course_reminder.com'
receivers = ['xk_cs@hnu.edu.cn', 'permission@hnu.edu.cn']

#para#3
#1:text content
#2:text format of plain
#3:set utf-8 code

cur_course = tools.__get_today_course__() 

msg_list = ['']

#convert nested list to single longer string 
for info in cur_course:
	msg_list.append(info + "\n")

msg_str = "".join(msg_list)

#debug print
print("Mail content :\n" + msg_str)


#html_msg = """
#<p>Python mail sender test...</p>
#<p><a href="https://www.baidu.com">百度</a></p>
#"""

#message = MIMEText(html_msg, 'html', 'utf-8')
message = MIMEText(msg_str, 'plain', 'utf-8')

message['From'] = Header("Server 41563 course reminder", 'utf-8') # sender
message['To'] = Header("Permission", 'utf-8') # receiver

subject = 'Server reminds you of your courses today'
message['Subject'] = Header(subject, 'utf-8')

try:
	smtpObj = smtplib.SMTP('localhost')
	smtpObj.sendmail(sender, receivers, message.as_string())
	print("Mail send done")
except smtplib.SMTPException:
	print ("Error:Can not send mail...")
